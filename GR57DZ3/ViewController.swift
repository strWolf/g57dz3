//
//  ViewController.swift
//  GR57DZ3
//
//  Created by student08 on 16.10.17.
//  Copyright © 2017 student04. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var inputText: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        /*
         1 создать строку с своим именем
         вывести количество символов содержащихся в ней
     */
        let  mayName = "Владими"
        var countInt = FuncViewController.countSymbol(str: mayName)
        print("Количество символов =", countInt)
        
        /*2 создать строку с своим отчеством
 проверить его на окончание “ич/на”
 (в классе написан метод который позволяет проверить на суффикс или префикс, найдите и используйте его)*/
        let fatherName = "Яремович"
        print("Отчество сожержит следуюшее окончание ",FuncViewController.endingFatherName(str: fatherName))
 
         /*3 создать строку где слитно написано Ваши ИмяФамилия “IvanVasilevich"
 разбить на две отдельных строки из предыдущей создать третью где они обе будут разделены пробелом
 str1 = “Ivan”
 str2 = “Vasilevich”
 str3 = “Ivan Vasilevich"
         (переменная вариант возрашает 0-Имя, 1-Фамилию, 2-Все вместе через пробел) */
        let str = "ВладимирРавлик"
        print(FuncViewController.returnNameSurname(str: str, variant: 2))
        
      /*
         *4 вывести строку зеркально Ось -> ьсО
         не используя reverse (посимвольно)     */
        let str1 = "Ось"
        print("\(str1) ->",FuncViewController.mirrorString(str: str1))
      
        /*
         *5 добавить запятые в строку как их расставляет калькулятор
         1234567 -> 1,234,567
         12345 -> 12,345
         (не использовать встроенный метод для применения формата)         */
          let str3 = "1234567"
          print("\(str3) -> ",FuncViewController.commasString(str: str3))
      
          /*
         *6 проверить пароль на надежность от 1 до 5
         a) если пароль содержит числа +1
         b) символы верхнего регистра +1
         c) символы нижнего регистра +1
         d) спец символы +1
         e) если содержит все вышесказанное         */
          let str2 = "12R$3fg5h"
          print("\(str2) -> ",FuncViewController.reliabilityPassword(str: str2))
        
        /*
         *7 сортировка массива не встроенным методом по возрастанию + удалить дубликаты
         */
       let arraySortDel = [2, 4, 2, 11]
       print(FuncViewController.arraySortAndDeletingDuplicstes(arrayStr: arraySortDel))
        
        /*
         *9 сделать выборку из массива строк в которых содержится указанная строка
         [“lada”, “sedan”, “baklazhan”] search “da”
         -> [“lada”, “sedan”] - sort() && sort using NSPredicate         */
        let arraySortDel1 = ["eerfsd", "deee", "sdswe4"];       print(FuncViewController.sampleArray(arrayStr: arraySortDel1, serchStr: "sd"))
        
        /*
         *8 написать метод который будет переводить строку в транслит - пример
         print(convertStrToTranslite(:”ЯЗЗЬ”)) -> “YAZZ”
         print(convertStrToTranslite:”морДа”) -> “morDa”         */
       print(FuncViewController.convertStrToTranslite(str: "ЯЗЗЬ"))
        
        
        
    }




}

