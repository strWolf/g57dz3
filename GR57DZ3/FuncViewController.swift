//
//  FuncViewController.swift
//  GR57DZ3
//
//  Created by student08 on 16.10.17.
//  Copyright © 2017 student04. All rights reserved.
//

import UIKit

class FuncViewController: UITableViewController {
    /*
     *1 создать строку с своим именем
     вывести количество символов содержащихся в ней
     (возможно не так понял задание но)
     */
    static func countSymbol(str: String)->Int{
        return str.count
    }
    
    /*
     *2 создать строку с своим отчеством
     проверить его на окончание “ич/на”
     (в классе написан метод который позволяет проверить на суффикс или префикс, найдите и используйте его)
     */
    static func endingFatherName(str: String)->String{
        var endingIch = "ич"
        var endingNa = "на"
        let index = str.index(str.startIndex, offsetBy: (str.count - 2))
        if (str.substring(from: index) == endingIch){
            return endingIch
        }
        else if (str.substring(from: index) == endingNa){
            return endingNa
            }
        else{
            return str.substring(from: index)
        }
        return "feiled"
    }
    
    
    static func analisisStrGreatLetters(str: String)->String{
        var returnStr = ""
        var arrayStr = Array(str)
        returnStr = returnStr + String(arrayStr[0])
        for i in 1..<str.count{
            if String(arrayStr[i]).uppercased() == String(arrayStr[i]){
                returnStr = returnStr + " " + String(arrayStr[i])
            }
            else{
                returnStr = returnStr + String(arrayStr[i])
           }
        }
        
        return returnStr
    }
    
    /*
     *3 создать строку где слитно написано Ваши ИмяФамилия “IvanVasilevich"
     разбить на две отдельных строки из предыдущей создать третью где они обе будут разделены пробелом
     str1 = “Ivan”
     str2 = “Vasilevich”
     str3 = “Ivan Vasilevich"
     */
    static func returnNameSurname (str: String, variant: Int)->String{
        var returnStr = analisisStrGreatLetters(str: str)
        var arStr = returnStr.split(separator: " ")
        if variant == 0{
            returnStr = String(arStr[0])
        }
        else if variant == 1{
            returnStr = String(arStr[1])
        }
        return returnStr
    }
    
    /*
     *4 вывести строку зеркально Ось -> ьсО
     не используя reverse (посимвольно)
     */
    static func mirrorString(str: String)->String{
        var arrayStr = Array(str)
        var returnStr = ""
        let countStr = arrayStr.count - 1
        for i in 0..<countStr+1 {
            let j = countStr - i
            returnStr = returnStr + String(arrayStr[j])
        }
        return returnStr
    }
    
    /*
     *5 добавить запятые в строку как их расставляет калькулятор
     1234567 -> 1,234,567
     12345 -> 12,345
     (не использовать встроенный метод для применения формата)
     */
    static func commasString(str: String) ->String{
        var returnStr = ""
        var arrayStr = Array(str)
        let countStr = str.count - 1
        var commas = 1
        for i in 0..<countStr+1 {
            let j = countStr - i
            if commas%3 == 0{
                returnStr = returnStr + String(arrayStr[j]) + ","
            }
            else{
                returnStr = returnStr + String(arrayStr[j])
            }
            commas += 1
        }
        return mirrorString(str: returnStr)
    }
    
    /*
     * если большой символ то возвращает 1
     */
    static func upperCase(str: String)->Int{
        if str.uppercased() == str {
            return 1
        }
        return 0
    }
    
    /*
     * если маленький символ то возвращает 1
     */
    static func lowerCase(str: String)->Int{
        if str.lowercased() == str {
            return 1
        }
        return 0
    }
    
    /*
     * если цифра символ то возвращает 1
     */
    static func numberCase(str: String)->Int{
        for j in 0..<10{
            if str == String(j){
                return 1
            }
        }
        return 0
    }
    
    /*
     * если спец символ то возвращает 1
     */
    static func specialCharacters(str: String)->Int{
        let specialChar = "~!@#$%^&*()_+}{?><,./;:[]=-`"
        for sC in specialChar{
            if str == String(sC){
                return 1
            }
        }
        return 0
    }
    
    /*
     *6 проверить пароль на надежность от 1 до 5
     a) если пароль содержит числа +1
     b) символы верхнего регистра +1
     c) символы нижнего регистра +1
     d) спец символы +1
     e) если содержит все вышесказанное
     */
    static func reliabilityPassword(str: String) ->String{
        var returnStr = ""
        var arrayStr = Array(str)
        let countStr = str.count - 1
        var flagA = 0, flagB = 0, flagC = 0, flagD = 0
        for i in 0..<countStr+1 {
            if specialCharacters(str: String(arrayStr[i])) == 1{
                flagD = 1
            }
            else if numberCase(str: String(arrayStr[i])) == 1{
               flagA = 1
            }
            else if upperCase(str: String(arrayStr[i])) == 1{
                flagB = 1
            }
            else if lowerCase(str: String(arrayStr[i])) == 1{
                flagC = 1
            }
        }
        let sumFlag = flagD + flagC + flagA + flagB
        if sumFlag == 4 {
            return "5 a) b) c) d) e)"
        }
        else{
            returnStr = String(sumFlag) + " "
            if flagA == 1 {
                returnStr += "a) "
            }
            if flagB == 1 {
                returnStr += "b) "
            }
            if flagC == 1 {
                returnStr += "c) "
            }
            if flagD == 1 {
                returnStr += "d) "
            }
        }
        return returnStr
    }
    
    /*
    *7 сортировка массива не встроенным методом по возрастанию + удалить дубликаты
     */
   static func arraySortAndDeletingDuplicstes(arrayStr:Array<Int>)->Array<Int>{
        var arrayReturn = Array<Int>()
        var arrayStrSet = Set<Int>()
        for str in arrayStr{
            arrayStrSet.insert(str)
        }
        arrayReturn = Array(arrayStrSet)
    
        let sizeArray = arrayReturn.count
        for i in 0..<sizeArray{
            let goOver = (sizeArray - 1) - i
            for j in 0..<goOver{
                let value = arrayReturn[j]
                if value > arrayReturn[j + 1]{
                    let temp = arrayReturn[j + 1]
                    arrayReturn[j + 1] = value
                    arrayReturn[j] = temp
                }
            }
        }
        return arrayReturn
    }
    
    /*
     * Проверка в нижнем регистре символ или в верхнем
     */
    static func testNotLargeSymbol(str: String)->Bool{
        if str == str.uppercased(){
            return false
        }
        return true
    }
    
    /*
     *8 написать метод который будет переводить строку в транслит - пример
     print(convertStrToTranslite(:”ЯЗЗЬ”)) -> “YAZZ”
     print(convertStrToTranslite:”морДа”) -> “morDa”
     */
    static func convertStrToTranslite(str: String)->String{
        var returnStr = ""
        var arrayTranscript: [String:String] = ["й" : "Y","ц" : "c",
                                                "у" : "u","е" : "e",
                                                "н" : "n","г" : "g",
                                                "ш" : "sh","щ" : "sha",
                                                "з" : "z","х" : "x",
                                                "ъ" : " ","ф" : "f",
                                                "ы" : " ","в" : "v",
                                                "а" : "a","п" : "p",
                                                "р" : "r","о" : "o",
                                                "л" : "l","д" : "d",
                                                "ж" : "zy","э" : " ",
                                                "я" : "ya","ч" : "ch",
                                                "с" : "s","м" : "m",
                                                "и" : "i","т" : "t",
                                                "ь" : " ","б" : "b",
                                                "ю" : "u"]
                                                
        let arrayStr = Array(str)
        for iSrt in arrayStr{
            if testNotLargeSymbol(str: String(iSrt)){
                returnStr = returnStr + arrayTranscript[String(iSrt)]!
            }
            else{
                returnStr = returnStr + String(arrayTranscript[String(iSrt).lowercased()]!).uppercased()
            }
        }
        return returnStr
    }
    
    /*
     *9 сделать выборку из массива строк в которых содержится указанная строка
     [“lada”, “sedan”, “baklazhan”] search “da”
     -> [“lada”, “sedan”] - sort() && sort using NSPredicate
     */
    static func sampleArray(arrayStr:Array<String>, serchStr: String)->Array<String>{
        var arrayReturn = Array<String>()
        var i = 0
        for str in arrayStr{
            if str.range(of: serchStr) == nil  {
                arrayReturn.insert(str, at: i)
                i += 1
            }
        }
        return arrayReturn
    }
}
